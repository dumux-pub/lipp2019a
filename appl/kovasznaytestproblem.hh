// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \brief Test for the staggered grid Navier-Stokes model with analytical solution (Kovasznay 1947)
 */
#ifndef DUMUX_KOVASZNAY_TEST_PROBLEM_HH
#define DUMUX_KOVASZNAY_TEST_PROBLEM_HH

#include <dune/alugrid/grid.hh>

#include <dumux/assembly/staggeredrefinedlocalresidual.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/constant.hh>

#include <dumux/freeflow/navierstokes/myproblem.hh>
#include <dumux/freeflow/navierstokes/model.hh>
#include <dumux/freeflow/navierstokes/mylocalresidual.hh>
#include <dumux/freeflow/navierstokes/myfluxvariables.hh>
#include <dumux/freeflow/navierstokes/myvolumevariables.hh>
#include <dumux/freeflow/navierstokes/myiofields.hh>

#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/discretization/staggered/freeflow/myfacevariables.hh>
#include <dumux/discretization/staggered/freeflow/myvelocityoutput.hh>
#include <dumux/discretization/staggered/freeflow/mygridvolumevariables.hh>
#include <dumux/discretization/staggered/freeflow/elementvolumevariables1.hh>
#include <dumux/discretization/staggered/myfacesolution.hh>
#include <dumux/discretization/staggered/mygridvariables.hh>
#include <dumux/discretization/staggered/myfvgridgeometry.hh>

namespace Dumux
{
template <class TypeTag>
class KovasznayTestProblem;

namespace Properties
{
NEW_TYPE_TAG(KovasznayTestTypeTag, INHERITS_FROM(StaggeredFreeFlowModel, NavierStokes));

// the fluid system
SET_PROP(KovasznayTestTypeTag, FluidSystem)
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar> >;
};

// Set the grid type
SET_TYPE_PROP(KovasznayTestTypeTag, Grid, Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming >);

// Set the problem property
SET_TYPE_PROP(KovasznayTestTypeTag, Problem, Dumux::KovasznayTestProblem<TypeTag> );

SET_BOOL_PROP(KovasznayTestTypeTag, EnableGridGeometryCache, true);

SET_BOOL_PROP(KovasznayTestTypeTag, EnableGridFluxVariablesCache, true);
SET_BOOL_PROP(KovasznayTestTypeTag, EnableGridVolumeVariablesCache, true);

//! The variables living on the faces
template<class TypeTag>
struct FaceVariables<TypeTag, TTag::KovasznayTestTypeTag>
{
private:
    using FacePrimaryVariables = GetPropType<TypeTag, Properties::FacePrimaryVariables>;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
public:
    using type = MyStaggeredFaceVariables<ModelTraits, FacePrimaryVariables, GridView::dimension, upwindSchemeOrder>;
};

//! The velocity output
template<class TypeTag>
struct VelocityOutput<TypeTag, TTag::KovasznayTestTypeTag>
{
    using type = MyStaggeredFreeFlowVelocityOutput<GetPropType<TypeTag, Properties::GridVariables>,
                                                 GetPropType<TypeTag, Properties::SolutionVector>>;
};

template<class TypeTag, class MyTypeTag>
struct FaceVolumeVariables { using type = UndefinedProperty; };                     //!< The secondary variables within a sub-control volume

//! Set the volume variables property
template<class TypeTag>
struct FaceVolumeVariables<TypeTag, TTag::KovasznayTestTypeTag>
{
private:
    using VV = GetPropType<TypeTag, Properties::VolumeVariables>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    static constexpr auto dimWorld = GridView::dimensionworld;
    static constexpr int numPairs = 2 * (dimWorld - 1);

public:
    using type = NavierStokesFaceVolumeVariables<VV, numPairs>;
};

//! Set the default global volume variables cache vector class
template<class TypeTag>
struct GridVolumeVariables<TypeTag, TTag::KovasznayTestTypeTag>
{
private:
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using FaceVolumeVariables = GetPropType<TypeTag, Properties::FaceVolumeVariables>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using SubControlVolume = typename GET_PROP_TYPE(TypeTag, GridGeometry)::SubControlVolume;
    using SubControlVolumeFace = typename GET_PROP_TYPE(TypeTag, GridGeometry)::SubControlVolumeFace;
    static constexpr auto enableCache = getPropValue<TypeTag, Properties::EnableGridVolumeVariablesCache>();
    using Traits = MyStaggeredGridDefaultGridVolumeVariablesTraits<Problem, FaceVolumeVariables, VolumeVariables, GridView, SubControlVolume, SubControlVolumeFace>;
public:
    using type = MyStaggeredGridVolumeVariables<Traits, enableCache>;
};

//! Set the BaseLocalResidual to StaggeredLocalResidual
template<class TypeTag>
struct BaseLocalResidual<TypeTag, TTag::KovasznayTestTypeTag> { using type = StaggeredRefinedLocalResidual<TypeTag>; };

//! Set the face solution type
template<class TypeTag>
struct StaggeredFaceSolution<TypeTag, TTag::KovasznayTestTypeTag>
{
private:
    using FaceSolutionVector = GetPropType<TypeTag, Properties::FaceSolutionVector>;
public:
    using type = Dumux::MyStaggeredFaceSolution<FaceSolutionVector>;
};

//! Set the grid variables (volume, flux and face variables)
template<class TypeTag>
struct GridVariables<TypeTag, TTag::KovasznayTestTypeTag>
{
private:
    using GG = GetPropType<TypeTag, Properties::GridGeometry>;
    using GVV = GetPropType<TypeTag, Properties::GridVolumeVariables>;
    using GFVC = GetPropType<TypeTag, Properties::GridFluxVariablesCache>;
    using GFV = GetPropType<TypeTag, Properties::GridFaceVariables>;
public:
    using type = MyStaggeredGridVariables<GG, GVV, GFVC, GFV>;
};



//! Set the volume variables property
template<class TypeTag>
struct VolumeVariables<TypeTag, TTag::KovasznayTestTypeTag>
{
private:
    using PV = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using FSY = GetPropType<TypeTag, Properties::FluidSystem>;
    using FST = GetPropType<TypeTag, Properties::FluidState>;
    using MT = GetPropType<TypeTag, Properties::ModelTraits>;

    static_assert(FSY::numPhases == MT::numFluidPhases(), "Number of phases mismatch between model and fluid system");
    static_assert(FST::numPhases == MT::numFluidPhases(), "Number of phases mismatch between model and fluid state");
    static_assert(!FSY::isMiscible(), "The Navier-Stokes model only works with immiscible fluid systems.");

    using Traits = NavierStokesVolumeVariablesTraits<PV, FSY, FST, MT>;
public:
    using type = MyNavierStokesVolumeVariables<Traits>;
};

//! The local residual
template<class TypeTag>
struct LocalResidual<TypeTag, TTag::KovasznayTestTypeTag> { using type = RefinedNavierStokesResidual<TypeTag>; };

//! The flux variables
template<class TypeTag>
struct FluxVariables<TypeTag, TTag::KovasznayTestTypeTag> { using type = RefinedNavierStokesFluxVariables<TypeTag>; };

//! The specific I/O fields
template<class TypeTag>
struct IOFields<TypeTag, TTag::KovasznayTestTypeTag> { using type = MyNavierStokesIOFields; };
}

/*!
 * \ingroup NavierStokesTests
 * \brief  Test problem for the staggered grid (Kovasznay 1947)
 * \todo doc me!
 */
template <class TypeTag>
class KovasznayTestProblem : public MyNavierStokesProblem<TypeTag>
{
    using ParentType = MyNavierStokesProblem<TypeTag>;

    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using GridGeometry = typename GET_PROP_TYPE(TypeTag, GridGeometry);
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    using ModelTraits = typename GET_PROP_TYPE(TypeTag, ModelTraits);
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;

    static constexpr auto dimWorld = GET_PROP_TYPE(TypeTag, GridView)::dimensionworld;
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using VelocityVector = Dune::FieldVector<Scalar, dimWorld>;
public:
    KovasznayTestProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry), eps_(1e-6)
    {
        printL2Error_ = getParam<bool>("Problem.PrintL2Error");

        kinematicViscosity_ = getParam<Scalar>("Component.LiquidKinematicViscosity", 1.0);
        Scalar reynoldsNumber = 1.0 / kinematicViscosity_;
        lambda_ = 0.5 * reynoldsNumber
                        - std::sqrt(reynoldsNumber * reynoldsNumber * 0.25 + 4.0 * M_PI * M_PI);

        using CellArray = std::array<unsigned int, dimWorld>;
        const auto numCells = getParam<CellArray>("Grid.Cells");

        cellSizeX_ = this->gridGeometry().bBoxMax()[0] / numCells[0];
    }

   /*!
     * \name Problem parameters
     */
    // \{

    bool shouldWriteRestartFile() const
    {
        return false;
    }

    void postTimeStep(const SolutionVector& curSol) const
    { }

   /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 298.0; }


   /*!
     * \brief Return the sources within the domain.
     *
     * \param globalPos The global position
     */
    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    {
        return NumEqVector(0.0);
    }

    // \}
   /*!
     * \name Boundary conditions
     */
    // \{

   /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        // set Dirichlet values for the velocity everywhere
        values.setDirichlet(Indices::velocityXIdx);
        values.setDirichlet(Indices::velocityYIdx);

        return values;
    }

   /*!
     * \brief Returns whether a fixed Dirichlet value shall be used at a given cell.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param scv The sub control volume
     * \param pvIdx The primary variable index in the solution vector
     */
    bool isDirichletCell(const Element& element,
                         const FVElementGeometry& fvGeometry,
                         const SubControlVolume& scv,
                         int pvIdx) const
    {
        // set fixed pressure in all cells at the left boundary
        auto isAtLeftBoundary = [&](const FVElementGeometry& fvGeometry)
        {
            if (fvGeometry.hasBoundaryScvf())
            {
                for (const auto& scvf : scvfs(fvGeometry))
                    if (scvf.boundary() && scvf.center()[0] < this->gridGeometry().bBoxMin()[0] + eps_)
                        return true;
            }
            return false;
        };
        return (isAtLeftBoundary(fvGeometry) && pvIdx == Indices::pressureIdx);
    }

   /*!
     * \brief Return dirichlet boundary values at a given position
     *
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition & globalPos) const
    {
        // use the values of the analytical solution
        return analyticalSolution(globalPos);
    }

   /*!
     * \brief Return the analytical solution of the problem at a given position
     *
     * \param globalPos The global position
     */
    PrimaryVariables analyticalSolution(const GlobalPosition& globalPos) const
    {
        Scalar xShift = this->gridGeometry().bBoxMin()[0] + 0.5;
        Scalar yShift = this->gridGeometry().bBoxMin()[1] + 0.5;

        Scalar x = globalPos[0] - xShift;
        Scalar y = globalPos[1] - yShift;

        PrimaryVariables values;
        values[Indices::pressureIdx] = 0.5 * (1.0 - std::exp(2.0 * lambda_ * x));
        values[Indices::velocityXIdx] = 1.0 - std::exp(lambda_ * x) * std::cos(2.0 * M_PI * y);
        values[Indices::velocityYIdx] = 0.5 * lambda_ / M_PI * std::exp(lambda_ * x) * std::sin(2.0 * M_PI * y);

        return values;
    }

    // \}

   /*!
     * \name Volume terms
     */
    // \{

   /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;
        values[Indices::pressureIdx] = 0.0;
        values[Indices::velocityXIdx] = 0.0;
        values[Indices::velocityYIdx] = 0.0;

        return values;
    }

   /*!
     * \brief Returns the analytical solution for the pressure
     */
    auto& getAnalyticalPressureSolution() const
    {
        return analyticalPressure_;
    }

   /*!
     * \brief Returns the analytical solution for the velocity
     */
    auto& getAnalyticalVelocitySolution() const
    {
        return analyticalVelocity_;
    }

   /*!
     * \brief Returns the analytical solution for the velocity at the faces
     */
    auto& getAnalyticalVelocitySolutionOnFace() const
    {
        return analyticalVelocityOnFace_;
    }

    /*!
     * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
     */
    void createAnalyticalSolution()
    {
        analyticalPressure_.resize(this->gridGeometry().numCellCenterDofs());
        analyticalVelocity_.resize(this->gridGeometry().numCellCenterDofs());
        analyticalVelocityOnFace_.resize(this->gridGeometry().numFaceDofs());

        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);
            for (auto&& scv : scvs(fvGeometry))
            {
                auto ccDofIdx = scv.dofIndex();
                auto ccDofPosition = scv.dofPosition();
                auto analyticalSolutionAtCc = analyticalSolution(ccDofPosition);

                // velocities on faces
                for (auto&& scvf : scvfs(fvGeometry))
                {
                    const auto faceDofIdx = scvf.dofIndex();
                    const auto faceDofPosition = scvf.center();
                    const auto dirIdx = scvf.directionIndex();
                    const auto analyticalSolutionAtFace = analyticalSolution(faceDofPosition);
                    analyticalVelocityOnFace_[faceDofIdx][dirIdx] = analyticalSolutionAtFace[Indices::velocity(dirIdx)];
                }

                analyticalPressure_[ccDofIdx] = analyticalSolutionAtCc[Indices::pressureIdx];

                for(int dirIdx = 0; dirIdx < ModelTraits::dim(); ++dirIdx)
                    analyticalVelocity_[ccDofIdx][dirIdx] = analyticalSolutionAtCc[Indices::velocity(dirIdx)];
            }
        }
    }

private:
    Scalar eps_;
    Scalar cellSizeX_;

    Scalar kinematicViscosity_;
    Scalar lambda_;
    bool printL2Error_;
    std::vector<Scalar> analyticalPressure_;
    std::vector<VelocityVector> analyticalVelocity_;
    std::vector<VelocityVector> analyticalVelocityOnFace_;
};
} //end namespace

#endif
