SUMMARY
=======
This is the DuMuX module containing the code for producing the results for the book chapter:

Melanie Lipp, Rainer Helmig (2020):<br> __A locally-refined locally-conservative quadtree finite-volume staggered-grid scheme__. In Lamanna, G.; Tonini, S.; Cossali, G.E. and Weigand, B. (Eds.): Dropet Interaction and Spray Processes. Springer, Heidelberg, Berlin.

Installation
============

The easiest way to install this module and its dependencies is to create a new
directory and clone this module:

```
mkdir New_Folder
cd New_folder
git clone https://git.iws.uni-stuttgart.de/dumux-pub/lipp2019a.git
```

After that, execute the file [installLipp2019a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Lipp2019a/raw/master/installLipp2019a.sh)

```
chmod u+x lipp2019a/installLipp2019a.sh
./lipp2019a/installLipp2019a.sh
```

This should automatically download all necessary modules and check out the correct versions.

Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir New_Folder
cd New_Folder
```

Download the container startup script by running
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/lipp2019a/-/raw/master/docker/docker_lipp2019a.sh
```

Open the Docker Container
```bash
bash docker_lipp2019a.sh open
```

Applications
============


__Building from source__:

In order run the simulations of the different cases navigate to the folder

```bash
cd lipp2019a/build-cmake/appl
```

Compile the programm:

```bash
make test_navierstokes_kovasznay
```

And choose the respective input files to run the programms:

* With refinement ```./test_navierstokes_kovasznay test_refined.input```
* Without refinement ```./test_navierstokes_kovasznay test_unrefined.input```
