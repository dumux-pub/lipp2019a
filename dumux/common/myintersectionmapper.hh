// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Common
 * \brief defines intersection mappers.
 */
#ifndef DUMUX_MYINTERSECTIONITERATOR_HH
#define DUMUX_MYINTERSECTIONITERATOR_HH

#include <vector>
#include <unordered_map>

#include <dune/grid/common/mcmgmapper.hh>
#include <dune/grid/common/rangegenerators.hh>
#include <dumux/common/intersectionmapper.hh>


namespace Dumux {

/*!
 * \ingroup Common
 * \brief defines an intersection mapper for mapping of global DOFs assigned
 *        to faces which also works for non-conforming grids and corner-point grids.
 */
template<class GridView>
class MyNonConformingGridIntersectionMapper : public NonConformingGridIntersectionMapper<GridView>
{
    using Intersection = typename GridView::Intersection;
    using GridIndexType = typename GridView::IndexSet::IndexType;
    using ParentType = NonConformingGridIntersectionMapper<GridView>;

public:
    //! Constructor
    MyNonConformingGridIntersectionMapper(const GridView& gridView)
    : ParentType(gridView), gridView_(gridView)
    {}

    //! intersection.indexInInside does not give an intersection index, but a facetIndex. E.g. in 2D the rectangular elements have 0,1,2,3 as indexInInside. This function is intended to, in the case of hanging nodes, also have e.g. 4 as possible indexInInside - considering intersections instead of facets.
    GridIndexType isIndexInInside(const Intersection& myIs) const
    {
        int localMyIsIdx = 0;
        for (const auto& is : intersections(this->gridView_, myIs.inside())){
            if (containerCmp(myIs.geometry().center(), is.geometry().center()))
            {
                return localMyIsIdx;
            }
            ++localMyIsIdx;
        }
        std::cout << "should not be after for loop in isIndexInInside in intersectionmapper!" << std::endl;
        return 0;//just to avoid compiler warning
    }

    GridIndexType isIndexInOutside(const Intersection& myIs) const
    {
        int localMyIsIdx = 0;
        for (const auto& is : intersections(this->gridView_, myIs.outside())){
            if (containerCmp(myIs.geometry().center(), is.geometry().center()))
            {
                return localMyIsIdx;
            }
            ++localMyIsIdx;
        }
        std::cout << "should not be after for loop in isIndexInOutside in intersectionmapper!" << std::endl;
        return 0;//just to avoid compiler warning
    }

private:
    const GridView gridView_;

};

} // end namespace Dumux

#endif
