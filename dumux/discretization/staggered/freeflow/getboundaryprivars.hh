// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::MyStaggeredGridVolumeVariables
 */
#ifndef DUMUX_DISCRETIZATION_STAGGERED_GRID_MY_GETBOUNDARYPRIVARS_HH
#define DUMUX_DISCRETIZATION_STAGGERED_GRID_MY_GETBOUNDARYPRIVARS_HH

#include <dune/common/exceptions.hh>

//! make the local view function available whenever we use this class
#include <dumux/discretization/localview.hh>
#include <dumux/discretization/staggered/elementsolution.hh>

namespace Dumux {
    //! Returns the primary  variales used for the boundary volVars and checks for admissable
    //! combinations for boundary conditions.
    template<class Problem, class CellCenterPrimaryVariables, class Element, class SubControlVolumeFace, class VolumeVariables>
    static auto getBoundaryPriVars(const Problem& problem,
                                               const CellCenterPrimaryVariables& sol,
                                               const Element& element,
                                               const SubControlVolumeFace& scvf,
                                               const VolumeVariables& volVars)
    {
        using PrimaryVariables = typename VolumeVariables::PrimaryVariables;
        using Indices = typename VolumeVariables::Indices;
        static constexpr auto dim = PrimaryVariables::dimension - CellCenterPrimaryVariables::dimension;
        static constexpr auto offset = dim;

        const auto bcTypes = problem.boundaryTypes(element, scvf);
        PrimaryVariables boundaryPriVars(0.0);

        // make sure to not use outflow BC for momentum balance
        for(int i = 0; i < dim; ++i)
        {
            if(bcTypes.isOutflow(Indices::velocity(i)))
                DUNE_THROW(Dune::InvalidStateException, "Outflow condition cannot be used for velocity. Set only a Dirichlet value for pressure instead.");
        }

        if(bcTypes.isOutflow(Indices::pressureIdx))
            DUNE_THROW(Dune::InvalidStateException, "Outflow condition cannot be used for pressure. Set only a Dirichlet value for velocity instead.");

        // Determine the pressure value at a boundary with a Dirichlet condition for velocity.
        // This just takes the value of the adjacent inner cell.
        if(bcTypes.isDirichlet(Indices::velocity(scvf.directionIndex())))
        {
            if(bcTypes.isDirichlet(Indices::pressureIdx))
                DUNE_THROW(Dune::InvalidStateException, "A Dirichlet condition for velocity must not be combined with a Dirichlet condition for pressure");
            else
                boundaryPriVars[Indices::pressureIdx] = sol[Indices::pressureIdx - offset];
                // TODO: pressure could be extrapolated to the boundary
        }

        // Determine the pressure value for a boundary with a Dirichlet condition for pressure.
        // Takes a value specified in the problem.
        if(bcTypes.isDirichlet(Indices::pressureIdx))
        {
            if(bcTypes.isDirichlet(Indices::velocity(scvf.directionIndex())))
                DUNE_THROW(Dune::InvalidStateException, "A Dirichlet condition for velocity must not be combined with a Dirichlet condition for pressure");
            else
                boundaryPriVars[Indices::pressureIdx] = problem.dirichlet(element, scvf)[Indices::pressureIdx];
        }

        // Return for isothermal single-phase systems ...
        if(CellCenterPrimaryVariables::dimension == 1)
            return boundaryPriVars;

        // ... or handle values for components, temperature, etc.
        for(int eqIdx = offset; eqIdx < PrimaryVariables::dimension; ++eqIdx)
        {
            if(eqIdx == Indices::pressureIdx)
                continue;

            if(bcTypes.isDirichlet(eqIdx))
                boundaryPriVars[eqIdx] = problem.dirichlet(element, scvf)[eqIdx];
            else if(bcTypes.isOutflow(eqIdx) || bcTypes.isSymmetry() || bcTypes.isNeumann(eqIdx))
                boundaryPriVars[eqIdx] = sol[eqIdx - offset];
        }
        return boundaryPriVars;
    }

} // end namespace Dumux

#endif
