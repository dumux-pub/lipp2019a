// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::MyStaggeredElementVolumeVariables
 */
#ifndef DUMUX_DISCRETIZATION_STAGGERED_MYELEMENT_VOLUMEVARIABLES_HH
#define DUMUX_DISCRETIZATION_STAGGERED_MYELEMENT_VOLUMEVARIABLES_HH

#include <algorithm>
#include <cassert>
#include <iterator>
#include <vector>

#include <dune/common/exceptions.hh>
#include <dumux/discretization/staggered/elementsolution.hh>
#include <dumux/discretization/staggered/freeflow/getboundaryprivars.hh>


namespace Dumux {
/*!
 * \ingroup StaggeredDiscretization
 * \brief Volume variables stored per sub face
 */
template<class VV, int numPairs>
struct NavierStokesFaceVolumeVariables
{
    using VolumeVariables = VV;
    using PrimaryVariables = typename VolumeVariables::PrimaryVariables;

    VolumeVariables inside;
    VolumeVariables virtualOppo;
    std::array<VolumeVariables, numPairs> normal;

    //! Update all volume variables
    template<class GridGeometry, class CellCenterPrimaryVariables, class CellCenterPrimaryVariablesArray, class SubControlVolumeFace, class Problem>
    void update(const GridGeometry& gridGeometry, const CellCenterPrimaryVariables& insideScvSol,  const std::vector<CellCenterPrimaryVariables>& insideScvSols, const CellCenterPrimaryVariablesArray& normalFaceOutsideSols, const SubControlVolumeFace& scvf, const Problem& problem)
    {
        using Scalar = typename GridGeometry::GridView::ctype;
        using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
        using ElementSolution = StaggeredElementSolution<PrimaryVariables>;
        using SubControlVolume = typename GridGeometry::SubControlVolume;

        std::vector<ElementSolution> insideElemSols;
        std::vector<Element> insideElements;
        std::vector<SubControlVolume> insideScvs;
        std::vector<Scalar> insideInterpolationFactors;

        for (unsigned int i = 0; i < scvf.volVarsData()[0/*pairIdx, does not matter for innerVolVarsDofs*/].innerVolVarsDofs.size(); ++i)
        {
            const auto scvIdx = scvf.volVarsData()[0].innerVolVarsDofs[i];

            if (! (scvIdx < 0))
            {
                const auto& insideScv = gridGeometry.scv(scvIdx);
                const auto& insideElement = gridGeometry.element(insideScv);
                // construct a privars object from the cell center solution vector
                const auto& cellCenterPriVarsInside = insideScvSols[i];
                PrimaryVariables priVarsInside = makePriVarsFromCellCenterPriVars<PrimaryVariables>(cellCenterPriVarsInside);
                auto insideElemSol = elementSolution<typename GridGeometry::LocalView>(std::move(priVarsInside));

                insideElemSols.push_back(insideElemSol);
                insideElements.push_back(insideElement);
                insideScvs.push_back(insideScv);
                insideInterpolationFactors.push_back(scvf.volVarsData()[0].innerVolVarsDofsInterpolationFactors[i]);
            }
            else
            {
                const auto& insideScv = gridGeometry.scv(scvf.insideScvIdx());
                const auto& insideElement = gridGeometry.element(insideScv);

                SubControlVolumeFace normalFace;
                bool didSetNormalFace = false;

                for (unsigned int localSubFaceIdx = 0; localSubFaceIdx < numPairs; ++localSubFaceIdx)
                {
                    auto fvGeometry = localView(gridGeometry);
                    fvGeometry.bindElement(insideElement);

                    const auto& testNormalFace = fvGeometry.scvf(scvf.insideScvIdx(), scvf.pairData(localSubFaceIdx).localNormalFluxCorrectionIndex);

                    if (testNormalFace.boundary())
                    {
                        normalFace = testNormalFace;
                        didSetNormalFace = true;
                    }
                }

                if (! didSetNormalFace)
                {
                    DUNE_THROW(Dune::InvalidStateException, "normal face was not set");
                }

                auto normalBoundaryPriVars = getBoundaryPriVars(problem, insideScvSols[0], gridGeometry.element(scvf.insideScvIdx()), normalFace, inside/*only workaround to get a type*/);

                const auto normalElemSol = elementSolution<typename GridGeometry::LocalView>(std::move(normalBoundaryPriVars));

                insideElemSols.push_back(normalElemSol);
                insideElements.push_back(insideElement);
                insideScvs.push_back(insideScv);
                insideInterpolationFactors.push_back(scvf.volVarsData()[0].innerVolVarsDofsInterpolationFactors[i]);
            }
        }

        inside.adaptiveUpdate(insideElemSols, problem, insideElements, insideScvs, insideInterpolationFactors);

        //outside, can be completely without interpolations, is for mass balance only
        if (scvf.boundary())
        {
            auto outsideBoundaryPriVars = getBoundaryPriVars(problem, insideScvSol, gridGeometry.element(scvf.insideScvIdx()), scvf, inside);
            const auto outsideElemSol = elementSolution<typename GridGeometry::LocalView>(std::move(outsideBoundaryPriVars));

            std::vector<ElementSolution> outsideElemSols;
            outsideElemSols.push_back(outsideElemSol);
            std::vector<Scalar> outsideInterpolationFactors;
            outsideInterpolationFactors.push_back(1.);

            virtualOppo.adaptiveUpdate(outsideElemSols, problem, insideElements, insideScvs, outsideInterpolationFactors);
        }

        //normal
        for (unsigned int localSubFaceIdx = 0; localSubFaceIdx < numPairs; ++localSubFaceIdx)
        {
            const auto eIdx = scvf.insideScvIdx();

            const auto& element = gridGeometry.element(scvf.insideScvIdx());
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bindElement(element);

            // Get the face normal to the face the dof lives on. The staggered sub face conincides with half of this normal face.
            const auto& normalFace = fvGeometry.scvf(eIdx, scvf.pairData(localSubFaceIdx).localNormalFluxCorrectionIndex);//localNormalFluxCorrectionIndex as one exemplary normalFace is sufficient to see if there is a neighbor (in the no-neighbor case there will be only one normalface per localSubFaceIdx anyway)

            if (normalFace.neighbor())
            {
                const auto& normalScv = gridGeometry.scv(normalFace.outsideScvIdx());
                const auto& normalElement = gridGeometry.element(normalScv);
                // construct a privars object from the cell center solution vector
                const auto& cellCenterPriVarsNormal = normalFaceOutsideSols[localSubFaceIdx];
                PrimaryVariables priVarsNormal = makePriVarsFromCellCenterPriVars<PrimaryVariables>(cellCenterPriVarsNormal);
                auto normalElemSol = elementSolution<typename GridGeometry::LocalView>(std::move(priVarsNormal));

                std::vector<ElementSolution> normalElemSols;
                normalElemSols.push_back(normalElemSol);
                std::vector<Element> normalElements;
                normalElements.push_back(normalElement);
                std::vector<SubControlVolume> normalScvs;
                normalScvs.push_back(normalScv);
                std::vector<Scalar> normalInterpolationFactors;
                normalInterpolationFactors.push_back(1.);

                normal[localSubFaceIdx].adaptiveUpdate(normalElemSols, problem, normalElements, normalScvs, normalInterpolationFactors);
            }
            else
            {
                auto outsideNormalBoundaryPriVars = getBoundaryPriVars(problem, insideScvSol, gridGeometry.element(scvf.insideScvIdx()), normalFace, inside);
                const auto outsideNormalElemSol = elementSolution<typename GridGeometry::LocalView>(std::move(outsideNormalBoundaryPriVars));

                std::vector<ElementSolution> outsideNormalElemSols;
                outsideNormalElemSols.push_back(outsideNormalElemSol);

                std::vector<Scalar> outsideNormalInterpolationFactors;
                outsideNormalInterpolationFactors.push_back(1.);

                normal[localSubFaceIdx].adaptiveUpdate(outsideNormalElemSols, problem, insideElements, insideScvs, outsideNormalInterpolationFactors);
            }
        }
    }
};

/*!
 * \ingroup StaggeredDiscretization
 * \brief Base class for the element volume variables vector for the staggered model
 */
template<class GVV, bool cachingEnabled>
class MyStaggeredElementVolumeVariables
{};

/*!
 * \ingroup StaggeredDiscretization
 * \brief Class for the element volume variables vector for the staggered model.
          Specialization in case the volume variables are stored globally.
 */
template<class GVV>
class MyStaggeredElementVolumeVariables<GVV, /*cachingEnabled*/true>
{
    using SubControlVolume = typename GVV::SubControlVolume;
    using SubControlVolumeFace = typename GVV::SubControlVolumeFace;

public:
    //! export type of the grid volume variables
    using GridVolumeVariables = GVV;

    //! export type of the volume variables
    using VolumeVariables = typename GridVolumeVariables::VolumeVariables;
    using FaceVolumeVariables = typename GridVolumeVariables::FaceVolumeVariables;

    //! Constructor
    MyStaggeredElementVolumeVariables(const GridVolumeVariables& gridVolVars)
    : gridVolVarsPtr_(&gridVolVars) {}

    //! operator for the access with an scvf
    const FaceVolumeVariables& operator [](const SubControlVolumeFace& scvf) const
    { return gridVolVars().volVars(scvf); }

    //! operator for the access with an scv
    const VolumeVariables& operator [](const SubControlVolume& scv) const
    { return gridVolVars().volVars(scv.dofIndex()); }

    //! operator for the access with an index
    //! needed for Staggered methods for the access to the boundary volume variables
    const VolumeVariables& operator [](const std::size_t scvIdx) const
    { return gridVolVars().volVars(scvIdx); }

    //! For compatibility reasons with the case of not storing the vol vars.
    //! function to be called before assembling an element, preparing the vol vars within the stencil
    template<class FVElementGeometry, class SolutionVector>
    void bind(const typename FVElementGeometry::GridGeometry::GridView::template Codim<0>::Entity& element,
              const FVElementGeometry& fvGeometry,
              const SolutionVector& sol)
    {}

    //! function to prepare the vol vars within the element
    template<class FVElementGeometry, class SolutionVector>
    void bindElement(const typename FVElementGeometry::GridGeometry::GridView::template Codim<0>::Entity& element,
                     const FVElementGeometry& fvGeometry,
                     const SolutionVector& sol)
    {}

    //! The global volume variables object we are a restriction of
    const GridVolumeVariables& gridVolVars() const
    { return *gridVolVarsPtr_; }

private:
    const GridVolumeVariables* gridVolVarsPtr_;
};


/*!
 * \ingroup StaggeredDiscretization
 * \brief Class for the element volume variables vector for the staggered model.
          Specialization in case the volume variables are not stored globally.
 */
template<class GVV>
class MyStaggeredElementVolumeVariables<GVV, /*cachingEnabled*/false>
{
    using Indices = typename GVV::Indices; //TODO: get them out of the volvars

public:
    //! export type of the grid volume variables
    using GridVolumeVariables = GVV;

    //! export type of the volume variables
    using VolumeVariables = typename GridVolumeVariables::VolumeVariables;

    //! Constructor
    MyStaggeredElementVolumeVariables(const GridVolumeVariables& gridVolVars)
    : gridVolVarsPtr_(&gridVolVars) {}

    //! Binding of an element, prepares the volume variables within the element stencil
    //! called by the local jacobian to prepare element assembly
    template<class FVElementGeometry, class SolutionVector, class SubControlVolumeFace>
    void bind(const typename FVElementGeometry::GridGeometry::GridView::template Codim<0>::Entity& element,
              const FVElementGeometry& fvGeometry,
              const SolutionVector& sol)
    {
        clear();

        const auto& problem = gridVolVars().problem();
        const auto& gridGeometry = fvGeometry.gridGeometry();
        const auto globalI = gridGeometry.elementMapper().index(element);
        const auto map = gridGeometry.connectivityMap();
        constexpr auto cellCenterIdx = FVElementGeometry::GridGeometry::cellCenterIdx();
        const auto& connectivityMapI = map(cellCenterIdx, cellCenterIdx, globalI);
        const auto numDofs = connectivityMapI.size();

        auto&& scvI = fvGeometry.scv(globalI);

        // resize local containers to the required size (for internal elements)
        volumeVariables_.resize(numDofs);
        volVarIndices_.resize(numDofs);
        int localIdx = 0;

        using CellCenterPrimaryVariables = typename SolutionVector::value_type;

        // Update the volume variables of the element at hand and the neighboring elements
        for (auto globalJ : connectivityMapI)
        {
            const auto& elementJ = gridGeometry.element(globalJ);
            auto&& scvJ = fvGeometry.scv(globalJ);
            CellCenterPrimaryVariables priVars(0.0);
            priVars = sol[cellCenterIdx][globalJ];
            auto elemSol = elementSolution<FVElementGeometry>(std::move(priVars));
            volumeVariables_[localIdx].update(elemSol,
                                              problem,
                                              elementJ,
                                              scvJ);
            volVarIndices_[localIdx] = scvJ.dofIndex();
            ++localIdx;
        }

        // Update boundary volume variables
        for (auto&& scvf : scvfs(fvGeometry))
        {
            // if we are not on a boundary, skip to the next scvf
            if (!scvf.boundary())
                continue;

            volumeVariables_.resize(localIdx+1);
            volVarIndices_.resize(localIdx+1);

            auto boundaryPriVars = GVV::Traits::getBoundaryPriVars(problem, sol, element, scvf);
            auto elemSol = elementSolution<FVElementGeometry>(std::move(boundaryPriVars));
            volumeVariables_[localIdx].update(elemSol,
                                              problem,
                                              element,
                                              scvI);
            volVarIndices_[localIdx] = scvf.outsideScvIdx();
             ++localIdx;
        }
    }

    //! Binding of an element, prepares only the volume variables of the element.
    //! Specialization for Staggered models
    template<class FVElementGeometry, class SolutionVector, class SubControlVolumeFace>
    void bindElement(const typename FVElementGeometry::GridGeometry::GridView::template Codim<0>::Entity& element,
                     const FVElementGeometry& fvGeometry,
                     const SolutionVector& sol)
    {
        clear();

        const auto eIdx = fvGeometry.gridGeometry().elementMapper().index(element);
        volumeVariables_.resize(1);
        volVarIndices_.resize(1);

        // update the volume variables of the element
        auto&& scv = fvGeometry.scv(eIdx);
        using CellCenterPrimaryVariables = typename SolutionVector::value_type;
        CellCenterPrimaryVariables priVars(0.0);
        constexpr auto cellCenterIdx = FVElementGeometry::GridGeometry::cellCenterIdx();
        priVars = sol[cellCenterIdx][eIdx];
        auto elemSol = elementSolution<FVElementGeometry>(std::move(priVars));
        volumeVariables_[0].update(elemSol,
                                   gridVolVars().problem(),
                                   element,
                                   scv);
        volVarIndices_[0] = scv.dofIndex();
    }

    //! const operator for the access with an scv
    template<class SubControlVolume, typename std::enable_if_t<!std::is_integral<SubControlVolume>::value, int> = 0>
    const VolumeVariables& operator [](const SubControlVolume& scv) const
    { return volumeVariables_[getLocalIdx_(scv.dofIndex())]; }

    //! operator for the access with an scv
    template<class SubControlVolume, typename std::enable_if_t<!std::is_integral<SubControlVolume>::value, int> = 0>
    VolumeVariables& operator [](const SubControlVolume& scv)
    { return volumeVariables_[getLocalIdx_(scv.dofIndex())]; }

    //! const operator for the access with an index
    const VolumeVariables& operator [](std::size_t scvIdx) const
    { return volumeVariables_[getLocalIdx_(scvIdx)]; }

    //! operator for the access with an index
    VolumeVariables& operator [](std::size_t scvIdx)
    { return volumeVariables_[getLocalIdx_(scvIdx)]; }

    //! The global volume variables object we are a restriction of
    const GridVolumeVariables& gridVolVars() const
    { return *gridVolVarsPtr_; }

    //! Clear all local storage
    void clear()
    {
        volVarIndices_.clear();
        volumeVariables_.clear();
    }

private:
    const GridVolumeVariables* gridVolVarsPtr_;

    int getLocalIdx_(const int volVarIdx) const
    {
        auto it = std::find(volVarIndices_.begin(), volVarIndices_.end(), volVarIdx);
        assert(it != volVarIndices_.end() && "Could not find the current volume variables for volVarIdx!");
        return std::distance(volVarIndices_.begin(), it);
    }

    std::vector<std::size_t> volVarIndices_;
    std::vector<VolumeVariables> volumeVariables_;
};

} // end namespace Dumux

#endif
