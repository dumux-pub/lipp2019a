// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StaggeredDiscretization
 * \copydoc Dumux::MyStaggeredGridVolumeVariables
 */
#ifndef DUMUX_DISCRETIZATION_STAGGERED_GRID_MYVOLUMEVARIABLES_HH
#define DUMUX_DISCRETIZATION_STAGGERED_GRID_MYVOLUMEVARIABLES_HH

#include <dune/common/exceptions.hh>

//! make the local view function available whenever we use this class
#include <dumux/discretization/localview.hh>
#include <dumux/discretization/staggered/elementsolution.hh>
#include <dumux/discretization/staggered/freeflow/elementvolumevariables1.hh>
#include <dumux/discretization/staggered/freeflow/getboundaryprivars.hh>

namespace Dumux {
template<class P, class FVV, class VV, class GV, class SCV, class SCVF>
struct MyStaggeredGridDefaultGridVolumeVariablesTraits
{
    using Problem = P;
    using VolumeVariables = VV;
    using PrimaryVariables = typename VV::PrimaryVariables;
    using GridView = GV;
    using SubControlVolume = SCV;
    using SubControlVolumeFace = SCVF;
    using FaceVolumeVariables = FVV;

    template<class GridVolumeVariables, bool cachingEnabled>
    using LocalView = MyStaggeredElementVolumeVariables<GridVolumeVariables, cachingEnabled>;
};

/*!
 * \ingroup StaggeredDiscretization
 * \brief Grid volume variables class for staggered models
 */
template<class Traits, bool cachingEnabled>
class MyStaggeredGridVolumeVariables;

/*!
 * \ingroup StaggeredDiscretization
 * \brief Grid volume variables class for staggered models.
          Specialization in case of storing the volume variables
 */
template<class Traits>
class MyStaggeredGridVolumeVariables<Traits, /*cachingEnabled*/true>
{
    using ThisType = MyStaggeredGridVolumeVariables<Traits, true>;
    using Problem = typename Traits::Problem;
    using PrimaryVariables = typename Traits::VolumeVariables::PrimaryVariables;
    using ElementSolution = StaggeredElementSolution<PrimaryVariables>;
    using GridView = typename Traits::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = typename GridView::ctype;

    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr int numPairs = 2 * (dimWorld - 1);

public:
    using SubControlVolume = typename Traits::SubControlVolume;
    using SubControlVolumeFace = typename Traits::SubControlVolumeFace;

    //! export the type of the indices
    using Indices = typename Traits::VolumeVariables::Indices;

    //! export the type of the VolumeVariables
    using VolumeVariables = typename Traits::VolumeVariables;
    using FaceVolumeVariables = typename Traits::FaceVolumeVariables;

    //! make it possible to query if caching is enabled
    static constexpr bool cachingEnabled = true;

    //! export the type of the local view
    using LocalView = typename Traits::template LocalView<ThisType, cachingEnabled>;

    MyStaggeredGridVolumeVariables(const Problem& problem) : problemPtr_(&problem) {}

    //! Update all volume variables
    template<class GridGeometry, class SolutionVector>
    void update(const GridGeometry& gridGeometry, const SolutionVector& sol)
    {
        auto numScv = gridGeometry.numScv();
        auto numScvf = gridGeometry.numScvf();
        auto numBoundaryScvf = gridGeometry.numBoundaryScvf();

        cellCenterVolumeVariables_.resize(numScv + numBoundaryScvf);
        faceVolumeVariables_.resize(numScvf + numBoundaryScvf);

        for (const auto& element : elements(gridGeometry.gridView()))
        {
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                // construct a privars object from the cell center solution vector
                const auto& cellCenterPriVars = sol[scv.dofIndex()];
                PrimaryVariables priVars = makePriVarsFromCellCenterPriVars<PrimaryVariables>(cellCenterPriVars);

                auto elemSol = elementSolution<typename GridGeometry::LocalView>(std::move(priVars));



                std::vector<ElementSolution> elemSols;
                elemSols.push_back(elemSol);
                std::vector<Element> elements;
                elements.push_back(element);
                std::vector<SubControlVolume> scvs;
                scvs.push_back(scv);
                std::vector<Scalar> interpolationFactors;
                interpolationFactors.push_back(1.);

                cellCenterVolumeVariables_[scv.dofIndex()].adaptiveUpdate(elemSols, problem(), elements, scvs, interpolationFactors);
            }

            for (auto&& scvf : scvfs(fvGeometry))
            {
                //inside
                std::vector<ElementSolution> insideElemSols;
                std::vector<Element> insideElements;
                std::vector<SubControlVolume> insideScvs;
                std::vector<Scalar> insideInterpolationFactors;

                for (unsigned int i = 0; i < scvf.volVarsData()[0/*pairIdx, does not matter for innerVolVarsDofs*/].innerVolVarsDofs.size(); ++i)
                {
                    const auto scvIdx = scvf.volVarsData()[0].innerVolVarsDofs[i];

                    if (! (scvIdx < 0))
                    {
                        const auto& insideScv = gridGeometry.scv(scvIdx);
                        const auto& insideElement = gridGeometry.element(insideScv);
                        // construct a privars object from the cell center solution vector
                        const auto& cellCenterPriVarsInside = sol[scvIdx];
                        PrimaryVariables priVarsInside = makePriVarsFromCellCenterPriVars<PrimaryVariables>(cellCenterPriVarsInside);
                        auto insideElemSol = elementSolution<typename GridGeometry::LocalView>(std::move(priVarsInside));

                        insideElemSols.push_back(insideElemSol);
                        insideElements.push_back(insideElement);
                        insideScvs.push_back(insideScv);
                        insideInterpolationFactors.push_back(scvf.volVarsData()[0].innerVolVarsDofsInterpolationFactors[i]);
                    }
                    else
                    {
                        SubControlVolumeFace normalFace;
                        bool didSetNormalFace = false;
                        int counter = 0;

                        for (unsigned int localSubFaceIdx = 0; localSubFaceIdx < numPairs; ++localSubFaceIdx)
                        {
                            const auto& testNormalFace = fvGeometry.scvf(scvf.insideScvIdx(), scvf.pairData(localSubFaceIdx).localNormalFluxCorrectionIndex);

                            if (testNormalFace.boundary())
                            {
                                normalFace = testNormalFace;
                                didSetNormalFace = true;
                                counter++;
                            }
                        }

                        if (! didSetNormalFace)
                        {
                            DUNE_THROW(Dune::InvalidStateException, "normal face was not set");
                        }

                        if (counter > 1)
                        {
                            DUNE_THROW(Dune::InvalidStateException, "boundary on both sides not allowed here");
                        }

                        const auto& insideScv = gridGeometry.scv(scvf.insideScvIdx());
                        const auto& insideElement = gridGeometry.element(insideScv);

                        auto normalBoundaryPriVars = getBoundaryPriVars(problem(), sol[scvf.insideScvIdx()], gridGeometry.element(scvf.insideScvIdx()), normalFace, cellCenterVolumeVariables_[0]/*only workaround to get a type*/);

                        const auto normalElemSol = elementSolution<typename GridGeometry::LocalView>(std::move(normalBoundaryPriVars));

                        insideElemSols.push_back(normalElemSol);
                        insideElements.push_back(insideElement);
                        insideScvs.push_back(insideScv);
                        insideInterpolationFactors.push_back(scvf.volVarsData()[0].innerVolVarsDofsInterpolationFactors[i]);
                    }
                }

                faceVolumeVariables_[scvf.index()].inside.adaptiveUpdate(insideElemSols, problem(), insideElements, insideScvs, insideInterpolationFactors);

                //outside, can be completely without interpolations, is for mass balance only
                if (scvf.boundary())
                {
                    auto outsideBoundaryPriVars = getBoundaryPriVars(problem(), sol[scvf.insideScvIdx()], gridGeometry.element(scvf.insideScvIdx()), scvf, cellCenterVolumeVariables_[0]/*only workaround to get a type*/);
                    const auto outsideElemSol = elementSolution<typename GridGeometry::LocalView>(std::move(outsideBoundaryPriVars));

                    std::vector<ElementSolution> outsideElemSols;
                    outsideElemSols.push_back(outsideElemSol);
                    std::vector<Scalar> outsideInterpolationFactors;
                    outsideInterpolationFactors.push_back(1.);

                    faceVolumeVariables_[scvf.index()].virtualOppo.adaptiveUpdate(outsideElemSols, problem(), insideElements, insideScvs, outsideInterpolationFactors);
                }

                //normal
                for (unsigned int localSubFaceIdx = 0; localSubFaceIdx < numPairs; ++localSubFaceIdx)
                {
                    const auto eIdx = scvf.insideScvIdx();
                    // Get the face normal to the face the dof lives on. The staggered sub face conincides with half of this normal face.
                    const auto& normalFace = fvGeometry.scvf(eIdx, scvf.pairData(localSubFaceIdx).localNormalFluxCorrectionIndex);//localNormalFluxCorrectionIndex as one exemplary normalFace is sufficient to see if there is a neighbor (in the no-neighbor case there will be only one normalface per localSubFaceIdx anyway)

                    if (normalFace.neighbor())
                    {
                        const auto& normalScv = gridGeometry.scv(normalFace.outsideScvIdx());
                        const auto& normalElement = gridGeometry.element(normalScv);

                        // construct a privars object from the cell center solution vector
                        const auto& cellCenterPriVarsNormal = sol[normalFace.outsideScvIdx()];
                        PrimaryVariables priVarsNormal = makePriVarsFromCellCenterPriVars<PrimaryVariables>(cellCenterPriVarsNormal);
                        auto normalElemSol = elementSolution<typename GridGeometry::LocalView>(std::move(priVarsNormal));

                        std::vector<ElementSolution> normalElemSols;
                        normalElemSols.push_back(normalElemSol);
                        std::vector<Element> normalElements;
                        normalElements.push_back(normalElement);
                        std::vector<SubControlVolume> normalScvs;
                        normalScvs.push_back(normalScv);
                        std::vector<Scalar> normalInterpolationFactors;
                        normalInterpolationFactors.push_back(1.);

                        faceVolumeVariables_[scvf.index()].normal[localSubFaceIdx].adaptiveUpdate(normalElemSols, problem(), normalElements, normalScvs, normalInterpolationFactors);
                    }
                    else
                    {
                        auto outsideNormalBoundaryPriVars = getBoundaryPriVars(problem(), sol[normalFace.insideScvIdx()], gridGeometry.element(scvf.insideScvIdx()), normalFace, cellCenterVolumeVariables_[0]/*only workaround to get a type*/);
                        const auto outsideNormalElemSol = elementSolution<typename GridGeometry::LocalView>(std::move(outsideNormalBoundaryPriVars));

                        std::vector<ElementSolution> outsideNormalElemSols;
                        outsideNormalElemSols.push_back(outsideNormalElemSol);

                        std::vector<Scalar> outsideNormalInterpolationFactors;
                        outsideNormalInterpolationFactors.push_back(1.);

                        faceVolumeVariables_[scvf.index()].normal[localSubFaceIdx].adaptiveUpdate(outsideNormalElemSols, problem(), insideElements, insideScvs, outsideNormalInterpolationFactors);
                    }
                }
            }

            // handle the boundary volume variables
            for (auto&& scvf : scvfs(fvGeometry))
            {
                // if we are not on a boundary, skip the rest
                if (!scvf.boundary())
                    continue;

                const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
                auto boundaryPriVars = getBoundaryPriVars(problem(), sol[scvf.insideScvIdx()], element, scvf, cellCenterVolumeVariables_[0]/*only workaround to get a type*/);
                const auto elemSol = elementSolution<typename GridGeometry::LocalView>(std::move(boundaryPriVars));

                std::vector<ElementSolution> elemSols;
                elemSols.push_back(elemSol);
                std::vector<Element> elements;
                elements.push_back(element);
                std::vector<SubControlVolume> insideScvs;
                insideScvs.push_back(insideScv);
                std::vector<Scalar> interpolationFactors;
                interpolationFactors.push_back(1.);

                cellCenterVolumeVariables_[scvf.outsideScvIdx()].adaptiveUpdate(elemSols, problem(), elements, insideScvs, interpolationFactors);
            }
        }
    }

//illustration of, why there need to exist volVars that depend not only on the scv but on the scvf and that differ inner and outer(localSubFaceIdx):
// -------------------------
// |     |     |     |     |
// |     |     |     |     |
// |     |     |     |     |
// ---------*******---------
// |     |  *  |  *  |     |
// |     |  *  |  *  |     |
// |     |  *  |  *  |     |
// ---------*******---------
// |     |     ||          |
// |     |     || x        |
// |     |     ||          |
// ------------||          |
// |     |     |           |
// |     |     |           |
// |     |     |           |
// -------------------------
// but
// -------------------------
// |     |     |     |     |
// |     |     |     |     |
// |     |     |     |     |
// -------------------------
// |     |     |     |     |
// |     |     |     |     |
// |     |     |     |     |
// ---------**********------
// |     |  *  ||    *     |
// |     |  *  ||    x     |
// |     |  *  ||    *     |
// ---------**********     |
// |     |     |           |
// |     |     |           |
// |     |     |           |
// -------------------------
//
//  ||: intersection
//  *: control volume
//  x: The position at which the volume variables are taken in the lower right cell, both times x corresponds to the lower right cell and to the same intersection, and the positions are still different depending if the the value is inner or outer.

    const FaceVolumeVariables& volVars(const SubControlVolumeFace& scvf) const
    { return faceVolumeVariables_[scvf.index()]; }

    FaceVolumeVariables& volVars(const SubControlVolumeFace& scvf)
    { return faceVolumeVariables_[scvf.index()]; }

    const VolumeVariables& volVars(const SubControlVolume& scv) const
    { return cellCenterVolumeVariables_[scv.dofIndex()]; }

    VolumeVariables& volVars(const SubControlVolume& scv)
    { return cellCenterVolumeVariables_[scv.dofIndex()]; }

    const VolumeVariables& volVars(const std::size_t scvIdx) const
    { return cellCenterVolumeVariables_[scvIdx]; }

    VolumeVariables& volVars(const std::size_t scvIdx)
    { return cellCenterVolumeVariables_[scvIdx]; }

    const Problem& problem() const
    { return *problemPtr_; }

private:
    const Problem* problemPtr_;
    std::vector<VolumeVariables> cellCenterVolumeVariables_;
    std::vector<FaceVolumeVariables> faceVolumeVariables_;
};


/*!
 * \ingroup StaggeredDiscretization
 * \brief Grid volume variables class for staggered models.
          Specialization in case of not storing the volume variables
 */
template<class Traits>
class MyStaggeredGridVolumeVariables<Traits, /*cachingEnabled*/false>
{
    using ThisType = MyStaggeredGridVolumeVariables<Traits, false>;
    using Problem = typename Traits::Problem;

public:
    //! export the type of the VolumeVariables
    using VolumeVariables = typename Traits::VolumeVariables;

    //! make it possible to query if caching is enabled
    static constexpr bool cachingEnabled = false;

    //! export the type of the local view
    using LocalView = typename Traits::template LocalView<ThisType, cachingEnabled>;

    MyStaggeredGridVolumeVariables(const Problem& problem) : problemPtr_(&problem) {}

    template<class GridGeometry, class SolutionVector>
    void update(const GridGeometry& gridGeometry, const SolutionVector& sol) {}

    const Problem& problem() const
    { return *problemPtr_;}

private:

    const Problem* problemPtr_;
};

} // end namespace Dumux

#endif
