// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Discretization
 * \brief Base class for a sub control volume face
 */
#ifndef DUMUX_DISCRETIZATION_REFINED_SUBCONTROLVOLUMEFACEBASE_HH
#define DUMUX_DISCRETIZATION_REFINED_SUBCONTROLVOLUMEFACEBASE_HH

#include <utility>
#include <dune/common/fvector.hh>

#include <dumux/discretization/subcontrolvolumefacebase.hh>

namespace Dumux {

/*!
 * \ingroup Discretization
 * \brief Base class for a sub control volume face, i.e a part of the boundary
 *        of a sub control volume we computing a flux on.
 * \tparam Imp the implementation
 * \tparam ScvGeometryTraits traits of this class
 */
template<class Imp, class ScvfGeometryTraits>
class RefinedSubControlVolumeFaceBase : public SubControlVolumeFaceBase<Imp, ScvfGeometryTraits>
{
    using Implementation = Imp;
    using GridIndexType = typename ScvfGeometryTraits::GridIndexType;

public:
    //! export the type used for global coordinates
    using GlobalPosition = typename ScvfGeometryTraits::GlobalPosition;
    //! state the traits public and thus export all types
    using Traits = ScvfGeometryTraits;

    //! Index of the outside sub control volume face
    GridIndexType outsideScvfIdx() const
    {
        return asImp_().outsideScvfIdx();
    }


private:
    const Implementation& asImp_() const
    { return *static_cast<const Implementation*>(this); }

    Implementation& asImp_()
    { return *static_cast<Implementation*>(this); }
};

} // end namespace Dumux

#endif
